﻿#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h> //vključena datoteka za uporabo funkcije "sleep"

// Deklaracija konsante
#define FLOOR_NUM 9

// Deklaracija uporabljenih metod
void set_correct_input_string();
void move(int chosen_floor_index);
void print_floor_message(int floor_index);
bool is_elevator_stopped_by_user();

// Deklaracija globalnih spremenljivk
char *floors_str[] = {"B3", "B2", "B1", "P", "1", "2", "3", "4", "5"}; // Kazalce uporabljamo za tip char za predstavitev stringov
int current_floor_index = 3;
int chosen_floor_index = -1; // Za obdelavo vnesenih podatkov uporabljamo numerične indekse podatkovnega niza string

char input_str[2]; // Spremenljivka za zapis informacij od uporabnika
char *stop = "s";  // Kazalce uporabljamo za tip "char" za predstavitev stringov

int main()
{
    set_correct_input_string();          // Z metodo preverimo pravilnost vnesenih podatkov
    while (strcmp(input_str, stop) != 0) // Program bo deloval do vnosa črke "S".
    {
        move(chosen_floor_index);
        set_correct_input_string();
    }
    printf("\nKonec programa \"Dvigalo\" 1.0.0 Platinum Edition\n");
    return 0;
}

void set_correct_input_string() // Način obveščanja uporabnika o možnostih vnosa
{
    // printf("\n");
    //  print_floor_message(current_floor_index);
    printf("\n*****\nPritisnite gumb za želeno etažo: P, 0-5, kleti B1-B3.\n Za dokončanje programa - s.\nŽelim iti v etažo številka: ");
    scanf("%s", input_str);
}

//
void print_floor_message(int chosen_floor_index)
{
    char *floor;
    if (chosen_floor_index != -1)
    {
        floor = floors_str[chosen_floor_index];
        printf("To je %s. nadstropje\n\n", floor);
    }
}

// Metoda za preverjanje pravilnosti vnesenih podatkov uporabnik,
// Za določanje smeri premikanja
// in prikaz poteka premikanja
void move(int chosen_floor_index)
{
    // int floor_index = -1;
    for (int i = 0; i < FLOOR_NUM; i++)
    {
        if (strcmp(floors_str[i], input_str) == 0) // strcmp - Za primerjavo uporabljamo spremenljivke tipa string
        {
            chosen_floor_index = i;
        }
    }
    if (chosen_floor_index == -1) // Če vhodnega elementa ni v polju "char *floors_str"...
    {
        printf("\n*****\nNapaka! V naši hiši ni takega nadstropja.\n P - pritličje,\n 1...5 - nadstropja,\n B1, B2, B3 - kleti.\n");
        sleep(4);
        return;
    }
    if (chosen_floor_index == current_floor_index)
    {
        printf("\n*****\nSte že v pravem nadstropju.\n");
        print_floor_message(current_floor_index);
        sleep(2);
        return;
    }

    printf("\n*****\nVrata so zaprta.\n\n");
    sleep(2);
    if (chosen_floor_index < current_floor_index)
    {
        for (int i = current_floor_index; i > chosen_floor_index; i--)
        {
            current_floor_index = i;

            print_floor_message(current_floor_index);

            if (is_elevator_stopped_by_user())
            {
                return;
            }
            sleep(1);
            printf("*****\n\\/ Dvigalo gre DOL!\n");
        }
    }
    if (chosen_floor_index > current_floor_index)
    {
        for (int i = current_floor_index; i < chosen_floor_index; i++)
        {
            current_floor_index = i;

            print_floor_message(current_floor_index);
            if (is_elevator_stopped_by_user())
            {
                return;
            }
            sleep(1);
            printf("\n*****\n/\\ Dvigalo gre GOR!\n");
        }
    }

    current_floor_index = chosen_floor_index;
    print_floor_message(current_floor_index);
    printf("*****\nPrišli ste.\nVrata so odprta.\n");
    print_floor_message(current_floor_index);
    sleep(3);
}

// Metoda za zaustavitev dvigala na poti do izbranega nadstropja.
bool is_elevator_stopped_by_user()
{
    int key = 0;

    while (1)
    {
        printf("\"x\" - če želite izstopiti v tem nadstropju\n\"Enter\" - če želite iti dlje\n");
        key = getchar(); // getchar - Prebere znak iz standardnega vnosa.
        if (key == 'x')
        {
            printf("\n*****\nPrišle ste.\nVrata so odprta.\n\n");
            sleep(4); // prekinitev programske niti
            return true;
        }
        else
        {
            return false;
        }
    }
}