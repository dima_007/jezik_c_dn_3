#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <conio.h>

// Deklaracija uporabljenih metod
int generate_index(int words_count); // Randomizer za izbiro besede iz nabora
void generate_guessing_array(bool *array);
void print_guessed(bool *guessing_array);
int calculate_guessed_letters(bool *guessing_array);
int input_letter();
bool is_letter_in_word(char letter, bool *guessing_array);
void print_attemptions();
void gallows_drawing();

// Deklaracija globalnih spremenljivk
char *word;
int length_of_selected_word;
int possibilities;
char attemptions[100];
int guessed_count;
int missed_count;

int main()
{
    int selected_option;
    // Niz besed "options" za igrico
    char *options[] = {"univerza", "slovenija", "programiranje", "jezik", "koper", "ljubljana", "ljubezenj"};

    selected_option = generate_index(7);    // Določanje vrstnega števila besede, ki jo uganemo
    word = options[selected_option];        // Določanje besede, ki jo uganemo
    length_of_selected_word = strlen(word); // strlen - določi dolžino niza "word"

    possibilities = 6; // Začetno Število dovoljenih poskusov

    bool guessing_array[length_of_selected_word];
    generate_guessing_array(guessing_array); // Ustvarimo začetno polje z manjkajočimi črkami
    print_guessed(guessing_array);           // Prikažemo igralno polje z ugankanimi

    for (int attemption = 1; attemption <= possibilities; attemption++) // Igra se nadaljuje, dokler niso izčrpane vse poskuske
    {
        char letter;
        printf("Attemption / Poskus: %d out of / od: %d:\n", attemption, possibilities);

        gallows_drawing();

        // if (attemption != 1) // Navodila in informacije o poskusih
        // {
        print_attemptions();
        //  }

        printf("\n*****\nInsert letter / Vnesite črko: ");
        letter = input_letter();

        if (is_letter_in_word(letter, guessing_array)) // Če je ta črka v besedi ali ni
        {
            printf("\nGOOD! \nLetter \"%c\" is guessed / Ta črka je v besedi.\n", letter);
        }
        else
        {
            printf("\nOH! NO! \nThere is no letter \"%c\" in the word. / Te črke ni v besedi.\n\n", letter);
            missed_count++;
        }

        attemptions[attemption - 1] = tolower(letter); // Funkcija tolower izvaja pretvorbo propisnih črk v stročnice.
        print_guessed(guessing_array);                 // Igralno polje posodabljamo po vnosu črk.

        int guessed_letters_count = calculate_guessed_letters(guessing_array);

        possibilities = 6 + guessed_letters_count; // Novo Število poskusov

        if (guessed_letters_count == length_of_selected_word)
        {
            printf("You won! / Zmagli ste! (: \n");
            return 0;
        }
    }
    gallows_drawing();
    printf("You lost! / Izgubili ste! :( \n");

    return 0;
}

/***********
 Metoda za branje črke
************/
int input_letter()
{
    int ch;

    ch = getchar();
    while ('\n' != getchar())
        ;

    return ch;
}

/***********
 Metoda za preverjanje črke v besedi
************/
bool is_letter_in_word(char letter, bool *guessing_array)
{
    bool is_exist = false;
    for (int i = 0; i < length_of_selected_word; i++)
    {
        if (tolower(word[i]) == tolower(letter)) // Funkcija tolower izvaja pretvorbo propisnih črk v stročnice.
        {
            guessing_array[i] = true;
            is_exist = true;
        }
        else
        {
        }
    }

    return is_exist;
}

int generate_index(int words_count) // Randomizer za izbiro besede iz nabora
{
    srand(time(0));
    return (rand() % words_count);
}

//
void generate_guessing_array(bool *array)
{
    for (int i = 0; i < length_of_selected_word; i++)
    {
        array[i] = false;
    }
}

/*************
 Metoda za prikaz igralnega polja z ugankanimi in manjkajočimi črkami
*************/
void print_guessed(bool *guessing_array)
{
    int guessed_letters_count = calculate_guessed_letters(guessing_array);

    for (int i = 0; i < length_of_selected_word; i++)
    {
        if (guessing_array[i])
        {
            printf("%c", word[i]);
        }
        else
        {
            printf("_");
        }
        printf("|");
    }

    printf("(%d/%d) \n", guessed_letters_count, length_of_selected_word);
}

/*************
 Metoda štetja uganljenih črk.
*************/
int calculate_guessed_letters(bool *guessing_array)
{
    int guessed_letters_count = 0;
    for (int i = 0; i < length_of_selected_word; i++)
    {
        if (guessing_array[i])
        {
            guessed_letters_count++;
        }
    }

    return guessed_letters_count;
}

/**************
 Metoda za fiksiranje črk, ki so bile vnesene prej
***************/
void print_attemptions()
{
    printf("You have tried / Poskušali ste vnesti črke: ");
    for (int i = 0; i < possibilities; i++)
    {
        printf("%c ", attemptions[i]);
    }
    printf("\n");
}

/*************
 Metoda za risanje vislic
**************/
void gallows_drawing()
{
    if (missed_count == 0)
    {
        printf("   _____\n ");
        printf(" |     |\n ");
        printf(" |\n ");
        printf(" |\n ");
        printf("/|\\ ");
        printf("\n");
    }
    if (missed_count == 1)
    {
        printf("   _____\n ");
        printf(" |     |\n ");
        printf(" |     0\n ");
        printf(" |\n ");
        printf("/|\\ ");
        printf("\n");
    }
    else if (missed_count == 2)
    {
        printf("   _____\n ");
        printf(" |     |\n ");
        printf(" |     0\n ");
        printf(" |     |\n ");
        printf("/|\\ ");
        printf("\n");
    }
    else if (missed_count == 3)
    {
        printf("   _____\n ");
        printf(" |     |\n ");
        printf(" |     0\n ");
        printf(" |    /|\n ");
        printf("/|\\ ");
        printf("\n");
    }
    else if (missed_count == 4)
    {
        printf("   _____\n ");
        printf(" |     |\n ");
        printf(" |     0\n ");
        printf(" |    /|\\ \n ");
        printf("/|\\");
        printf("\n");
    }
    else if (missed_count == 5)
    {
        printf("   _____\n ");
        printf(" |     |\n ");
        printf(" |     0\n ");
        printf(" |    /|\\ \n ");
        printf("/|\\   /");
        printf("\n");
    }
    else if (missed_count > 5)
    {
        printf("   _____\n ");
        printf(" |     |\n ");
        printf(" |     0\n ");
        printf(" |    /|\\ \n ");
        printf("/|\\   / \\ ");
        printf("\n");
    }
}